<?php
namespace Kowal\HomePageBaners\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;


class HomePageBaners extends Template implements BlockInterface
{

    protected $_template = "widget/homepagebaners.phtml";

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * HomePageBaners constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    )
    {
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    public function getBanerData($group_id)
    {
        return [
            'title' => $this->_scopeConfig->getValue('settings/' . $group_id . '/title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'subtitle' => $this->_scopeConfig->getValue('settings/' . $group_id . '/subtitle', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'button' => $this->_scopeConfig->getValue('settings/' . $group_id . '/button', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'url' => $this->_scopeConfig->getValue('settings/' . $group_id . '/url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'image' => "/media/homepage/".$this->_scopeConfig->getValue('settings/' . $group_id . '/image', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'bgcolor' => $this->_scopeConfig->getValue('settings/' . $group_id . '/bgcolor', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)
        ];
    }


}

