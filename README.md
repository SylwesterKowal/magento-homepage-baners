# Mage2 Module Kowal HomePageBaners

    ``kowal/module-homepagebaners``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Dodanie banerów i opisów do banerów na stronie głównej

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_HomePageBaners`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-homepagebaners`
 - enable the module by running `php bin/magento module:enable Kowal_HomePageBaners`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - title (settings/mainbaner/title)

 - subtitle (settings/mainbaner/subtitle)

 - button (settings/mainbaner/button)

 - url (settings/mainbaner/url)

 - image (settings/mainbaner/image)

 - title (settings/left1/title)

 - subtitle (settings/left1/subtitle)

 - button (settings/left1/button)

 - url (settings/left1/url)

 - image (settings/left1/image)

 - title (settings/left2/title)

 - subtitle (settings/left2/subtitle)

 - button (settings/left2/button)

 - url (settings/left2/url)

 - image (settings/left2/image)

 - title (settings/right1/title)

 - subtitle (settings/right1/subtitle)

 - button (settings/right1/button)

 - url (settings/right1/url)

 - image (settings/right1/image)

 - title (settings/right2/title)

 - subtitle (settings/right2/subtitle)

 - button (settings/right2/button)

 - url (settings/right2/url)

 - image (settings/right2/image)

 - title (settings/center/title)

 - subtitle (settings/center/subtitle)

 - button (settings/center/button)

 - url (settings/center/url)

 - image (settings/center/image)


## Specifications

 - Widget
	- HomePageBaners


## Attributes



